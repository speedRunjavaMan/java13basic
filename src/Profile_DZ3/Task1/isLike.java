package Profile_DZ3.Task1;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface isLike {
    public String name();
    public boolean like();
    public String value();
}

