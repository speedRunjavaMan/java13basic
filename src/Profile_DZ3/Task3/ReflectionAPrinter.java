package Profile_DZ3.Task3;

import java.lang.reflect.*;

public class ReflectionAPrinter {
    public void reflection() {
        Class<APrinter> cls = APrinter.class;
        Method method;
        try {
            method = cls.getMethod("print", int.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        try {
            method.invoke(cls, 123);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
