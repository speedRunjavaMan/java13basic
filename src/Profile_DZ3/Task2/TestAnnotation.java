package Profile_DZ3.Task2;

import Profile_DZ3.Task1.isLike;

import java.lang.annotation.Annotation;

public class TestAnnotation {
    public static void main(String[] args) {

        Class<?> aClass = SimpleClass.class;
        Annotation[] annotations = aClass.getAnnotations();

        for (Annotation annotation : annotations) {
            if(annotation instanceof isLike myAnnotation) {
                System.out.println("name: " + myAnnotation.name());
                System.out.println("value: " + myAnnotation.value());
                System.out.println("boolean: " + myAnnotation.like());
            }
        }
    }
}
