package Profile_DZ3.Task4;

import java.util.*;

public class ReflectionSuperClass {
    public static void main(String[] args) {
        List<Class<?>> result = getAllInterfaces(D.class);
        List<Class<?>> interfacesOfInterfaces = new ArrayList<>();
        for (Class<?> anInterface : result) {
            interfacesOfInterfaces.addAll(Arrays.asList(anInterface.getInterfaces()));
            System.out.println(anInterface.getName());
        }
        for (Class<?> anInterface : interfacesOfInterfaces) {
            System.out.println(anInterface.getName());
        }
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}
