package DZ3_part_3.Task3;

import java.util.ArrayList;
import java.util.Scanner;

public class Matrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        ArrayList<Integer> matrixN = new ArrayList<>();
        ArrayList<Integer> matrixM = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            matrixM.add(i);
            for (int j = 0; j < n; j++) {
                matrixN.add(j);
                System.out.print(matrixM.get(i) +  matrixN.get(j));
            }
            System.out.println();
        }
    }
}
