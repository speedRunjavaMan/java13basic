package DZ3_part_3.Task4;

import java.util.*;

public class Competition {

    public static void printWinners() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] participantName = new String[n];
        String[] dogName = new String[n];
        double[] averageGrade = new double[n];
        double[][] grades = new double[n][3];

        ArrayList<Participant> list = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            participantName[i] = scanner.next();
        }

        for (int j = 0; j < n; j++) {
            dogName[j] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                grades[i][j] = scanner.nextInt();
            }
        }

        for (int k = 0; k < n; k++) {
            double sum = 0;
            for (int i = 0; i < 3; i++) {
                sum += grades[k][i];
            }
            averageGrade[k] = sum / 3 * 10 / 10.0;
        }

        for (int i = 0; i < n; i++) {
            list.add(new Participant(new Dog(dogName[i]), participantName[i], averageGrade[i]));
        }

        list.sort(Comparator.comparingDouble(Participant::getGrade).reversed());

        for (int i = 0; i < 3; i++) {
            System.out.println(list.get(i).getName() + ": " + list.get(i).getDog().getName() + ", " + (int) (list.get(i).getGrade() * 10) / 10.0);
        }
    }
}