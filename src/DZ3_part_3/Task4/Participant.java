package DZ3_part_3.Task4;

public class Participant {
    private String name;
    private Dog dog;

    public Participant(Dog dog, String name, double grade) {
        this.dog = dog;
        this.name = name;
        Grade = grade;
    }

    private double Grade;

    public Dog getDog() {
        return dog;
    }

    public double getGrade() {
        return Grade;
    }

    public String getName() {
        return name;
    }

}
