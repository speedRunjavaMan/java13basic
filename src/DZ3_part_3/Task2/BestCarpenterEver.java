package DZ3_part_3.Task2;

public class BestCarpenterEver {

    private static boolean isStool;

    public static void setStool(boolean stool) {
        isStool = stool;
    }

    public static void checkFurnature(Object object) {
        if (object instanceof Stool) {
            ((Stool) object).wayOfUsing();
            setStool(true);
            System.out.println(isStool);
        } else {
            System.out.println("Sorry, can't fix it");
        }
    }
}