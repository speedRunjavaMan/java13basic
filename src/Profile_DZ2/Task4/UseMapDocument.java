package Profile_DZ2.Task4;

import java.util.*;

public class UseMapDocument {
    public static void main(String[] args) {
        Document doc1 = new Document("first doc", 123);
        Document doc2 = new Document("second doc", 231);
        Document doc3 = new Document("third doc", 321);
        List<Document> list = new ArrayList<>();
        list.add(doc1);
        list.add(doc2);
        list.add(doc3);
        MapDocument make = new MapDocument();
        Map<Integer, Document> m = make.organizeDocuments(list);
        System.out.println(m.get(123).name);
    }
}
