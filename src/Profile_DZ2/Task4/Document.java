package Profile_DZ2.Task4;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(String name, int id) {
        this.name = name;
        this.id = id;
    }
}
