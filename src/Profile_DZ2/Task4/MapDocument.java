package Profile_DZ2.Task4;

import java.util.*;

public class MapDocument {

    public Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document d : documents) {
            map.put(d.id, d);
        }
        return map;
    }
}
