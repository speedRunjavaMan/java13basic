package Profile_DZ2.Task2;


import java.util.*;


public class Anagram {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.next();
        if (checkAnagram(s, t))
            System.out.println(s + " and " + t + " - Anagrams");
        else
            System.out.println(s + " and " + t + " - NOT Anagrams");
    }

    public static boolean checkAnagram(String s1, String s2) {
        if (s1.length() != s2.length())
            return false;
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s1.length(); i++) {
            char x = s1.charAt(i);
            if (map.containsKey(x))
                map.put(x, map.get(x) + 1);
            else
                map.put(x, 1);
        }
        for (int i = 0; i < s2.length(); i++) {
            char y = s2.charAt(i);
            if (map.containsKey(y)) {
                if (map.get(y) == 1)
                    map.remove(y);
                else
                    map.put(y, map.get(y) - 1);
            } else
                return false;
        }
        return map.size() <= 0;
    }
}

