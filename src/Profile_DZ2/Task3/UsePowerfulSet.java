package Profile_DZ2.Task3;

import java.util.HashSet;
import java.util.Set;

public class UsePowerfulSet {
    public static void main(String[] args) {
        Set<Integer> s = new HashSet<>();
        s.add(1);
        s.add(2);
        s.add(3);
        Set<Integer> d = new HashSet<>();
        d.add(0);
        d.add(1);
        d.add(2);
        d.add(4);
        PowerfulSet<Integer> t = new PowerfulSet<>();
        System.out.println(t.intersection(s, d));
        System.out.println(t.union(s, d));
        System.out.println(t.relativeCompliment(s, d));
    }
}