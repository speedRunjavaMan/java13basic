package Profile_DZ2.Task3;

import java.util.*;


public class PowerfulSet<T> {
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        var result = new HashSet<>(set1);
        result.retainAll(set2);
        return result;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        var result = new HashSet<>(set1);
        result.addAll(set2);
        return result;
    }

    public <T> Set<T> relativeCompliment(Set<T> set1, Set<T> set2) {
        var result = new HashSet<>(set1);
        result.removeAll(set2);
        return result;
    }
}
