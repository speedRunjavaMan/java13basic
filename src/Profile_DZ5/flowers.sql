create table flowers
(
    id    serial primary key,
    title varchar(50) not null,
    price integer not null
);
create table orders
(
    id         bigserial primary key,
    flower_id  integer references flowers (id),
    quantity   integer     not null,
    name       varchar(50) not null,
    phone      varchar(50) not null,
    date_order timestamp   not null
);

-- check table data (empty first)
select *
from flowers;
select *
from orders;

-- insert data
insert into flowers(title, price)
values ('Roses', 100);
insert into flowers(title, price)
values ('Lilies', 50);
insert into flowers(title, price)
values ('Camomiles', 25);
insert into orders(flower_id, quantity, name, phone, date_order)
values (3, 100, 'Ivan', '+79532255566', now());
insert into orders(flower_id, quantity, name, phone, date_order)
values (2, 50, 'Petr', '+79213332211', now());
insert into orders(flower_id, quantity, name, phone, date_order)
values (1, 150, 'Fedor', '+79055055050', now());
insert into orders(flower_id, quantity, name, phone, date_order)
values (1, 50, 'Ivan', '+79532255566', now());

-- DZ Task1
select r.id, title, quantity, name, phone, date_order
from orders r inner join flowers f on r.flower_id = f.id
where r.id = 3;

-- DZ Task2
select *
from orders d
where d.date_order >= '2023-01-01'
and d.date_order < '2023-01-24'
and d.name = 'Ivan';

-- DZ Task3
select max(quantity) as "Maximum order", f.title as "Flower name"
from orders r inner join flowers f on r.flower_id = f.id
group by f.title;

-- DZ Task4
select sum("Total sum of flowers order") from
(select sum(quantity * price) as "Total sum of flowers order"
from orders r inner join flowers f on r.flower_id = f.id
group by f.title) as sum;

