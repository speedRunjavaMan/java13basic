package Profile_DZ4.Task2;

import java.util.List;

public class MultiplyStream {
    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5, 6);
        int mult = list
                .stream()
                .mapToInt(Integer::intValue)
                .reduce(1, (x, y) -> x * y);
        System.out.println(mult);
    }
}
