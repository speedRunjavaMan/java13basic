package Profile_DZ4.Task4;

import java.util.List;

public class RealNumbersSortStream {
    public static void main(String[] args) {
        List<Double> list = List.of(2.0, 3.0, 1.0, 5.0, 10.0, 6.0);
        list
                .stream()
                .mapToDouble(Double::doubleValue)
                .sorted()
                .forEach(System.out::println);
    }
}
