package Profile_DZ4.Task1;

import java.util.*;

public class SumStream {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            list.add(i);
        }
        int sum = list
                .stream()
                .mapToInt(Integer::intValue)
                .filter(digit -> digit % 2 == 0)
                .sum();

        System.out.println(sum);


    }
}
