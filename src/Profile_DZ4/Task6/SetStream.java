package Profile_DZ4.Task6;


import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SetStream {
    public static void main(String[] args) {
        Set<Integer> s1 = Set.of(1, 2, 3);
        Set<Integer> s2 = Set.of(4, 5, 6);
        Set<Set<Integer>> set = new HashSet<>();
        set.add(s1);
        set.add(s2);
        Set<Integer> flatSet = set
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        System.out.println(flatSet);
    }
}
