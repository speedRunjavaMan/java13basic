package Profile_DZ4.Task5;

import java.util.List;
import java.util.stream.Collectors;

public class UpperCaseStringStream {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        String joined = list.stream()
                .map(Object::toString)
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));
        System.out.println(joined);
    }
}
