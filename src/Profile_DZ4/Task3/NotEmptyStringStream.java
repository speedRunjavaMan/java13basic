package Profile_DZ4.Task3;

import java.util.List;

public class NotEmptyStringStream {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        long count = list
                .stream()
                .filter(string -> !(string.equals("")))
                .count();
        System.out.println(count);
    }
}
