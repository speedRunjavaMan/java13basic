package Profile_DZ1.Task4;

public class MyEvenNumber {
    private int n;

    public void setN(int n) {
        this.n = n;
    }

    public MyEvenNumber(int number) throws MyEvenNumberException {
        if (number % 2 == 0) {
            n = number;
            System.out.println("Instance of MyEvenNumber was created");
        } else throw new MyEvenNumberException("Could not create ");
    }
}


