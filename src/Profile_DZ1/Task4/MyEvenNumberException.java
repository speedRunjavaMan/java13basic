package Profile_DZ1.Task4;

public class MyEvenNumberException extends Exception{
    public MyEvenNumberException() {
    }

    public MyEvenNumberException(String message) {
        super(message);
    }
}
