package Profile_DZ1.Task1;

import java.util.Scanner;

public class Array {
    public static void tofillArray() throws MyCheckedException {
        int[] a = {1, 2, 3};
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter index: ");
        int index = scanner.nextInt();
        System.out.println("Enter value: ");
        int x = scanner.nextInt();
        try {
            a[index] = 5;
        } catch (IndexOutOfBoundsException e) {
            throw new MyCheckedException("Index is not valid");
        }
    }
}
