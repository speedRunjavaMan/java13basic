package Profile_DZ1.Task2;

public class MyUncheckedException extends RuntimeException {
    public MyUncheckedException() {
    }

    public MyUncheckedException(String message) {
        super(message);
    }

}
