package Profile_DZ1.Task2;

public class Circle {
    private double radius;

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getSquare() {
        if (radius <= 0) {
            throw new MyUncheckedException("Incorrect value of radius");
        } else return radius * radius * Math.PI;
    }
}
