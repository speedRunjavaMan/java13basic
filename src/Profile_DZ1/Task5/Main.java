package Profile_DZ1.Task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int n = inputN();
    }

    private static int inputN() {
        System.out.println("Enter a number n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        try {
            if (n < 100 && n > 0) {
                System.out.println("Correct input!");
            } else throw new Exception("Incorrect input!");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return n;
    }
}
