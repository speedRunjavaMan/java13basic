package Profile_DZ1.Task6;

import java.util.Scanner;

public class FormValidator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        checkName(scanner.nextLine());
        checkBirthdate(scanner.nextLine());
        checkGender(scanner.nextLine());
        checkHeight(scanner.nextLine());
    }

    static public void checkName(String str) {
        try {
            if (str.matches("[A-Z�-�][a-z�-�]{1,19}")) {
                System.out.println(true);
            } else {
                throw new Exception("Incorrect name input ");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static public void checkBirthdate(String str) {
        try {
            if (str.matches("([0-2]\\d|3[01])\\.(0\\d|1[0-2])\\.(19\\d\\d|(20([0-1]\\d|2[0-2])))")) {
                System.out.println(true);
            } else {
                throw new Exception("Incorrect date of Birth input ");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static public void checkGender(String str) {
        boolean b = false;
        for (Gender c : Gender.values()) {
            if (c.name().equals(str)) {
                b = true;
                break;
            }
        }
        try {
            if (b) {
                System.out.println(true);
            } else {
                throw new Exception("Incorrect gender input ");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    static public void checkHeight(String str) {
        try {
            double n = Double.parseDouble(str);
            if (n > 0) {
                System.out.println(true);
            } else {
                throw new Exception("Incorrect height input ");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
