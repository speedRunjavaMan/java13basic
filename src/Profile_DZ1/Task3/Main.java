package Profile_DZ1.Task3;

import java.io.*;
import java.util.*;


public class Main {
    public static void main(String[] args) {
        readWriteFile();
    }

    public static void readWriteFile() {
        try (Scanner scanner = new Scanner(new File("C:/SomeDir/input.txt"))) {
            List<String> list = new ArrayList<>();
            while (scanner.hasNextLine()) {
                list.add(scanner.nextLine());
            }
            try (FileOutputStream fos = new FileOutputStream("C:/SomeDir/output.txt")) {
                for (String line : list) {
                    byte[] buffer = (line.toUpperCase() + "\n").getBytes();
                    fos.write(buffer);

                }
            } catch (IOException e) {
                System.out.println("FileNotFoundException");
            }
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException");
        }
    }
}

