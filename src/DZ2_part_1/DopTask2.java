package DZ2_part_1;

import java.util.Scanner;

public class DopTask2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = (int) Math.pow(scanner.nextInt(), 2);
        }
        boolean s = false;
        int temp;
        while (!s) {
            s = true;
            for (int i = 0; i < b.length - 1; i++) {
                if (b[i] > b[i + 1]) {
                    temp = b[i];
                    b[i] = b[i + 1];
                    b[i + 1] = temp;
                    s = false;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(b[i] + " ");
        }
    }
}

