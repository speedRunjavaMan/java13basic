package DZ3_part_1.Task5;

public class DayOfWeek {
    private byte n;
    private String name;

    public byte getN() {
        return n;
    }

    public String getName() {
        return name;
    }

    public DayOfWeek(byte n, String name) {
        this.n = n;
        this.name = name;
    }
}
