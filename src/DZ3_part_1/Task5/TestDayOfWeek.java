package DZ3_part_1.Task5;

import java.util.ArrayList;

public class TestDayOfWeek {
    public static void main(String[] args) {

        ArrayList<DayOfWeek> day = new ArrayList<>();

        day.add(new DayOfWeek((byte) 1, "Monday"));
        day.add(new DayOfWeek((byte) 2, "Tuesday"));
        day.add(new DayOfWeek((byte) 3, "Wednesday"));
        day.add(new DayOfWeek((byte) 4, "Thursday"));
        day.add(new DayOfWeek((byte) 5, "Friday"));
        day.add(new DayOfWeek((byte) 6, "Saturday"));
        day.add(new DayOfWeek((byte) 7, "Sunday"));

        for (int i = 0; i < 7; i++) {
            System.out.println(day.get(i).getN() + " " + day.get(i).getName());
        }
    }
}
