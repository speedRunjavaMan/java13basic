package DZ3_part_1.Task2;

public class Student {

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    private String name;

    private String surname;

    private int[] grades = new int[10];

    public int[] addNewGrade(int newGrade) {
        for (int i = 1; i < grades.length; i++) {
            grades[i - 1] = grades[i];
        }
        grades[grades.length - 1] = newGrade;
        return grades;
    }

    int sum = 0;

    public int getAverageGrade() {
        for (int grade : grades) {
            sum += grade;
        }
        return sum / grades.length;
    }
}