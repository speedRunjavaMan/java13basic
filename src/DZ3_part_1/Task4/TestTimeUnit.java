package DZ3_part_1.Task4;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TestTimeUnit {
    public static void main(String[] args) {

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("HH:mm:");
        SimpleDateFormat dateFormat3 = new SimpleDateFormat("HH:");
        SimpleDateFormat dateFormat4 = new SimpleDateFormat("hh:mm:ss a");

        GregorianCalendar timeUnit = new GregorianCalendar(2022, Calendar.NOVEMBER, 11);

        timeUnit.set(Calendar.HOUR, 23);
        timeUnit.set(Calendar.MINUTE, 42);
        timeUnit.set(Calendar.SECOND, 12);

        System.out.println(dateFormat1.format(timeUnit.getTime()));
        System.out.println(dateFormat2.format(timeUnit.getTime()) + "00");
        System.out.println(dateFormat3.format(timeUnit.getTime()) + "00" + ":00");
        System.out.println(dateFormat4.format(timeUnit.getTime()));
        System.out.println();

        timeUnit.roll(Calendar.HOUR, 1);
        timeUnit.roll(Calendar.MINUTE, 5);
        timeUnit.roll(Calendar.SECOND, 3);

        System.out.println(dateFormat1.format(timeUnit.getTime()));
        System.out.println(dateFormat2.format(timeUnit.getTime()) + "00");
        System.out.println(dateFormat3.format(timeUnit.getTime()) + "00" + ":00");
        System.out.println(dateFormat4.format(timeUnit.getTime()));


        /*        TimeUnit time = new TimeUnit("08", "15", "30");
        System.out.print(" " + time.getHours());
        System.out.print(":" + time.getMinutes());
        System.out.print(":" + time.getSeconds());

        TimeUnit time1 = new TimeUnit("08", "15");
        System.out.print(" " + time1.getHours());
        System.out.print(":" + time1.getMinutes());
        System.out.print(":" + time1.s);

        TimeUnit time2 = new TimeUnit("08");
        System.out.print(" " + time2.getHours());
        System.out.print(":" + time2.m);
        System.out.print(":" + time2.s);
        System.out.println();*/
    }
}
