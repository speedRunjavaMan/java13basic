package DZ3_part_1.Task4;


public class TimeUnit {
    String s;
    String m;

    public String getHours() {
        return hours;
    }

    public String getMinutes() {
        return minutes;
    }

    public String getSeconds() {
        return seconds;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public void setSeconds(String seconds) {
        this.seconds = seconds;
    }

    private String hours;
    private String minutes;
    private String seconds;


    public TimeUnit(String h, String m, String s) {
        this.hours = h;
        this.minutes = m;
        this.seconds = s;
    }

    public TimeUnit(String h, String m) {
        this.hours = h;
        this.minutes = m;
        this.s = "00";
    }

    public TimeUnit(String h) {
        this.hours = h;
        this.m = "00";
        this.s = "00";
    }
}

