package DZ3_part_1.Task3;

import DZ3_part_1.Task2.Student;

import java.util.ArrayList;

public class StudentService {

    public void bestStudent(ArrayList<Student> a) {
        int AG1 = a.get(0).getAverageGrade();
        int AG2 = a.get(1).getAverageGrade();
        int AG3 = a.get(2).getAverageGrade();
        int AGMax = Math.max((Math.max(AG1, AG2)), AG3);
        if (AG1 == AG2 && AG2 == AG3) {
            System.out.println(a.get((int) (Math.random() * 3)).getSurname() + " " + AGMax);
        } else if (AGMax == AG1) {
            System.out.println(a.get(0).getSurname() + " " + AGMax);
        } else if (AGMax == AG2) {
            System.out.println(a.get(1).getSurname() + " " + AGMax);
        } else {
            System.out.println(a.get(2).getSurname() + " " + AGMax);
        }
    }

    public ArrayList<Student> sortBySurname(ArrayList<Student> a) {
        char S1 = (a.get(0).getSurname()).charAt(0);
        char S2 = (a.get(1).getSurname()).charAt(0);
        char S3 = (a.get(2).getSurname()).charAt(0);

        ArrayList<Student> courseSortedBySurname = new ArrayList<>();
        if (S1 < S2 && S1 < S3) {
            courseSortedBySurname.add(a.get(0));
            if (S2 < S3) {
                courseSortedBySurname.add(a.get(1));
                courseSortedBySurname.add(a.get(2));
            } else {
                courseSortedBySurname.add(a.get(2));
                courseSortedBySurname.add(a.get(1));
            }
        } else if (S2 < S1 && S2 < S3) {
            courseSortedBySurname.add(a.get(1));
            if (S1 < S3) {
                courseSortedBySurname.add(a.get(0));
                courseSortedBySurname.add(a.get(2));
            } else {
                courseSortedBySurname.add(a.get(2));
                courseSortedBySurname.add(a.get(0));
            }
        } else if (S3 < S1 && S3 < S2) {
            courseSortedBySurname.add(a.get(2));
            if (S1 < S2) {
                courseSortedBySurname.add(a.get(0));
                courseSortedBySurname.add(a.get(1));
            } else {
                courseSortedBySurname.add(a.get(1));
                courseSortedBySurname.add(a.get(0));
            }
        }
        return courseSortedBySurname;
    }
}
