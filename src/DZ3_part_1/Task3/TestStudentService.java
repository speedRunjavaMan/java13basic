package DZ3_part_1.Task3;

import DZ3_part_1.Task2.Student;

import java.util.ArrayList;

public class TestStudentService {
    public static void main(String[] args) {
// �������� 3� ���������
// �������� ������� ��������
        Student student0 = new Student();
        student0.setName("John");
        student0.setSurname("Dow");
        int[] gradesLast1 = {1, 1, 5, 2};
        student0.setGrades(gradesLast1);
// �������� ������� ��������
        Student student1 = new Student();
        student1.setName("Agent");
        student1.setSurname("Smith");
        int[] gradesLast2 = {1, 1, 5, 2};
        student1.setGrades(gradesLast2);
// �������� �������� ��������
        Student student2 = new Student();
        student2.setName("Keanu");
        student2.setSurname("Reeves");
        int[] gradesLast3 = {1, 10, 5, 2};
        student2.setGrades(gradesLast3);
//�������� ������������ ������ ���������
        ArrayList<Student> course = new ArrayList<>();
        course.add(student0);
        course.add(student1);
        course.add(student2);
//�������� ��������� ������ StudentService � ������� ������ ��� ����������� ������� �������� �� �������� ����� �
// ����������� ������ ��������� �� ��������
        StudentService rank = new StudentService();
        rank.bestStudent(course);
        rank.sortBySurname(course);
        System.out.println(rank.sortBySurname(course).get(0).getSurname());
        System.out.println(rank.sortBySurname(course).get(1).getSurname());
        System.out.println(rank.sortBySurname(course).get(2).getSurname());
    }
}
