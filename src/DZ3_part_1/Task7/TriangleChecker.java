package DZ3_part_1.Task7;

public class TriangleChecker {
    public static boolean isTriangle(double a, double b, double c) {
        return a + b > c && a + c > b && c + b > a;
    }
}
