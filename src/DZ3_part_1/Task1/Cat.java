package DZ3_part_1.Task1;

public class Cat {
    private static void sleep() {
        System.out.println("Sleep");
    }

    private static void meow() {
        System.out.println("Meow");
    }

    private static void eat() {
        System.out.println("Eat");
    }

    public static void status() {
        int s = (int) (Math.round(Math.random() * 2));

        switch (s) {
            case 0 -> Cat.sleep();
            case 1 -> Cat.meow();
            case 2 -> Cat.eat();
        }
    }
}
