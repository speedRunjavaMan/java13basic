package DZ3_part_1.Task8;

public class Atm {
    private static int numberOfAtm = 0;

    public int numberOfAtm() {
        return numberOfAtm;
    }

    double r;
    double u;

    public Atm(double rubcourse, double usdcourse) {
        r = rubcourse;
        u = usdcourse;
        numberOfAtm++;
    }

    public double convertUSDtoRUB(double usd) {
        return usd * r;
    }

    public double convertRUBtoUSD(double rub) {
        return rub * u;
    }

}
