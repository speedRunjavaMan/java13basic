package DZ3_part_1.Task6;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AmazingString {
    private char[] symbol;
    private String string;

    public AmazingString(char[] symbol) {
        this.symbol = symbol;
    }

    public AmazingString(String string) {
        this.string = string;
    }

    public void printString() {
        System.out.println(string);
    }

    public void printSimbol(int i) {
        StringBuilder s = new StringBuilder();
        System.out.println(s.append("" + string).charAt(i));
    }

    public void reverseString() {
        StringBuilder s = new StringBuilder();
        System.out.println(s.append("" + string).reverse());
    }

    public void lengthString() {
        System.out.println(string.toCharArray().length);
        StringBuilder s = new StringBuilder();
    }

    public void trimHeadSpaces() {
        char[] a = (string).toCharArray();

        int index = 0;

        for (int i = 0; i < a.length; i++) {
            if (a[i] != (' ')) {
                index = i;
                break;
            }
        }
        for (int i = index; i < a.length; i++) {
            System.out.print(a[i]);
        }
        System.out.println();
    }

    public void matchesString(String r) {

        String text = this.string;
        Pattern pattern = Pattern.compile(r);
        Matcher matcher = pattern.matcher(text);
        System.out.println(matcher.find());
    }

    public void matchesChar(char[] symbol, char[] arr) {
        boolean matchWord = false;
        for (int p = 0; p < arr.length; p++) {
            for (int i = 0; i < symbol.length && p + i < arr.length; i++) {
                if (symbol[i] != arr[p + i]) {
                    break;
                } else if (i + 1 == symbol.length) {
                    matchWord = true;
                }
            }
        }
        System.out.println(matchWord);
    }
}
