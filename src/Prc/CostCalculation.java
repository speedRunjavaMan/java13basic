package Prc;

import java.util.Scanner;

public class CostCalculation {
    static final int DISCOUNT_FIVE = 5;
    static final double VALUE_ADDED_TAX = 20;


    public static void main(String[] args) {

            double unitCost;
            int amount;
            String includeVatResponse;
            double fullCost;

            Scanner scanner = new Scanner(System.in);

            System.out.println("������� ��������� ������� ������");
            unitCost = scanner.nextDouble();

            System.out.println("������� ���������� ������");
            amount = scanner.nextInt();

            do {
                System.out.print("�������� ��� � ���������? (y/n): ");
                includeVatResponse = scanner.next();
            } while (!"y".equalsIgnoreCase(includeVatResponse)
                    && !"n".equalsIgnoreCase(includeVatResponse));

            if ("y".equalsIgnoreCase(includeVatResponse)) {
                if (amount >= 10) {
                    fullCost = getSumCost(unitCost, amount, DISCOUNT_FIVE, VALUE_ADDED_TAX);
                } else {
                    fullCost = getSumCost(unitCost, amount, VALUE_ADDED_TAX);
                       }
            } else {
                if (amount >= 10) {
                    fullCost = getSumCost(unitCost, amount, DISCOUNT_FIVE);
                } else {
                    fullCost = getSumCost(unitCost, amount);
                       }
            }
            System.out.println("�������� ��������� ������� �����: " + fullCost);
        }

        public static double getSumCost(double unitCost, int amount) {
            return unitCost * amount;
        }

        public static double getSumCost(double unitCost, int amount, int discount) {
            double priceWithDiscount = unitCost * (1 - discount / 100.0) * amount;
            return Math.round(priceWithDiscount * 100) / 100.0;
        }

        public static double getSumCost(double unitCost, int amount, double valueAddedTax) {
            double priceWithoutVAT = unitCost * amount;
            double priceWithVAT = priceWithoutVAT * (1 + valueAddedTax / 100.0);
            return Math.round(priceWithVAT * 100) / 100.0;
        }

        public static double getSumCost(double unitCost, int amount, int discount, double valueAddedTax) {
            double priceWithoutVAT = unitCost * (1 - discount / 100.0) * amount;
            double priceWithVAT = priceWithoutVAT * (1 + valueAddedTax / 100.0);
            return Math.round(priceWithVAT * 100) / 100.0;
        }


    }



