package DZ3_part_2;

public class TestLibrary {

    public static void main(String[] args) {
        Library lib = new Library();
        Visitor visitor1 = new Visitor("Ivan");
        Visitor visitor2 = new Visitor("Alexandr");
        Visitor visitor3 = new Visitor("Maria");
        Visitor visitor4 = new Visitor("Darya");
        Visitor visitor5 = new Visitor("Moisei Solomonovich");

        Book book1 = new Book("It", "Stephen King");
        Book book2 = new Book("Harry Potter and the Philosopher's Stone", "J. K. Rowling");
        Book book3 = new Book("A Fire Upon the Deep", "Vernor Vinge");
        Book book4 = new Book("Thinking in Java", "Bruce Eckel");
        Book book5 = new Book("Java for Dummies", "Barry Burd");
        Book book6 = new Book("Carrie", "Stephen King");
        Book book7 = new Book("Salem's Lot", "Stephen King");
        Book book8 = new Book("The Shining", "Stephen King");
        Book book9 = new Book("The Stand", "Stephen King");
        Book book10 = new Book("Pet Sematary", "Stephen King");

        lib.addBook("It", book1);
        lib.addBook("It", book1);
        lib.addBook("Harry Potter and the Philosopher's Stone", book2);
        lib.addBook("A Fire Upon the Deep", book3);
        lib.addBook("Thinking in Java", book4);
        lib.addBook("Java for Dummies", book5);
        lib.addBook("Carrie", book6);
        lib.addBook("Salem's Lot", book7);
        lib.addBook("The Shining", book8);
        lib.addBook("The Stand", book9);
        lib.addBook("Pet Sematary", book10);


        lib.removeBook("It", book1);
        lib.addBook("It", book1);

        lib.findBookByName("Harry Potter and the Philosopher's Stone");
        lib.findBookByName("Harry Potter");

        lib.findBookByAuthor("Stephen Kin");
        lib.findBookByAuthor("Stephen King");
        lib.takeBook("It", book1, visitor1);
        lib.takeBook("A Fire Upon the Deep", book3, visitor1);
        lib.returnTheBook("It", book1, visitor1);
        lib.returnTheBook("A Fire Upon the Deep", book3, visitor1);
        lib.takeBook("A Fire Upon the Deep", book3, visitor2);
        lib.returnTheBook("A Fire Upon the Deep", book3, visitor2);
        lib.takeBook("A Fire Upon the Deep", book3, visitor3);
        lib.returnTheBook("A Fire Upon the Deep", book3, visitor3);
    }
}
