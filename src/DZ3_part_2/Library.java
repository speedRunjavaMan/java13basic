package DZ3_part_2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Library {
    Scanner scanner = new Scanner(System.in);

    static Map<String, Book> mapOfBooks = new HashMap<>();

    static int numberOfVisitors;

    public void addBook(String name, Book book) {
        if (!mapOfBooks.containsKey(name)) {
            mapOfBooks.put(name, book);
            System.out.println("����� ������� ���������.");
        } else {
            System.out.println("����� ����� ��� ���� � ����������. �������� ���������� ������ �����.");
        }
    }

    public void removeBook(String name, Book book) {
        if (mapOfBooks.containsKey(name) && !(book.isBorrowed())) {
            mapOfBooks.remove(name, book);
            System.out.println("����� ������� �������.");
        }
    }

    public void findBookByName(String name) {
        if (mapOfBooks.containsKey(name)) {
            System.out.println("��� ����� ������� �� ������");
            mapOfBooks.get(name).print();
        } else {
            System.out.println("��������, ����� � ����� ��������� � ���������� ���. ��������� ��� �� � �������� ������ ��� ���������� ����� ������ �����.");
        }
    }

    public void findBookByAuthor(String author) {
        boolean a = false;
        for (Map.Entry<String, Book> entry : mapOfBooks.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue().getAuthor();
            if (value.equals(author)) {
                System.out.println(value + " - " + key);
                a = true;
            }
        }
        if (!a) {
            System.out.println("��������, ������ ������ � ���������� ���. ��������� ��� �� � ��� ������ ������ ��� ���������� ����� ������� ������.");
        }
    }

    public void returnTheBook(String name, Book book, Visitor visitor) {
        if (mapOfBooks.containsKey(name) && mapOfBooks.get(name).isBorrowed() && book.getVisitorsId() == visitor.getId()) {
            System.out.println("�������, ��� ������� �����!");
            book.print();
            book.setBorrowed(false);
            visitor.setBorrowedBook(false);
            book.times++;
            System.out.println("������� ������ ��� ����� �� ����� �� 1 �� 5");
            book.setAverageGrade(scanner.nextInt());
            book.getAverageGrade();
        }
    }

    public void takeBook(String name, Book book, Visitor visitor) {
        if (Library.mapOfBooks.containsKey(name) && !(Library.mapOfBooks.get(name).isBorrowed()) && !visitor.isBorrowedBook()) {
            System.out.println("��� ���� �����");
            book.print();
            book.setBorrowed(true);
            visitor.setBorrowedBook(true);
        } else if (visitor.isBorrowedBook()) {
            System.out.println("�������� � ��� ��� ���� ���� �����");
        } else if (Library.mapOfBooks.get(name).isBorrowed()) {
            System.out.println(!(Library.mapOfBooks.get(name).isBorrowed()));
            System.out.println("�������� ��� ����� ��������");
        }
        if (!visitor.isIfIdExist()) {
            Library.numberOfVisitors++;
            visitor.setId(Library.numberOfVisitors);
            System.out.println("����� ���������� � �����������. ��� id = " + visitor.getId());
            visitor.setIfIdExist(true);
        } else {
            System.out.println("�� ��� ����������������. ��� id = " + visitor.getId());
        }
        book.setVisitorsId(visitor.getId());
    }
}


