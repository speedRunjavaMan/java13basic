package DZ3_part_2;

public class Visitor {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Visitor(String name) {
    }

    private boolean borrowedBook;

    public boolean isBorrowedBook() {
        return borrowedBook;
    }

    private int id;
    private boolean ifIdExist;

    public void setIfIdExist(boolean ifIdExist) {
        this.ifIdExist = ifIdExist;
    }

    public boolean isIfIdExist() {
        return ifIdExist;
    }

    public void setBorrowedBook(boolean borrowedBook) {
        this.borrowedBook = borrowedBook;
    }

}